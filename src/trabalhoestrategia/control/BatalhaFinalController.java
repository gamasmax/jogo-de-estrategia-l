/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import trabalhoestrategia.model.Arqueiro;
import trabalhoestrategia.model.Bastiao;
import trabalhoestrategia.model.Exercito;
import trabalhoestrategia.model.Guerreiro;
import trabalhoestrategia.model.Lanceiro;
import trabalhoestrategia.model.Unidade;
import trabalhoestrategia.model.Zombie;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;
import javafx.scene.media.AudioClip;
/**
 * FXML Controller class
 *
 * @author Raynan
 */
public class BatalhaFinalController implements Initializable {

    /**
     * Initializes the controller class.
     */    
    
    @FXML
    private Label vidaAliado;
    @FXML
    private Label vidaInimigo;
    @FXML
    private Label vidaAliadoTotal;
    @FXML
    private Label vidaInimigoTotal;
    @FXML
    private Label zombies;
    @FXML
    private Circle bolinha1;
    @FXML
    private Circle bolinha2;
    @FXML
    private Circle bolinha3;
    @FXML
    private Circle bolinha4;
    @FXML
    private Circle bolinha5;
    
    @FXML
    private TextField velocidade; 
    
    @FXML
    private Label aliadosGuerreiros;
    @FXML
    private Label aliadosLanceiros;
    @FXML
    private Label aliadosBastioes;
    @FXML
    private Label aliadosArqueiros;
            
    private AudioClip clip;
    
    private boolean cura=false;
    
    private int contadorBastiao=101;
    private int contadorCura=101;
    private int numG=0;
    private int numB=0;
    private int numL=0;
    private int numA=0;
    private int numBastiaoEspecial=0;
    private Exercito amigo = new Exercito();
    private Exercito inimigo = new Exercito();
    
    private String AUDIO_URL = getClass().getResource("exorcist.mp3").toString();

    private int numZombies=50;
    private int contadorZombies=numZombies;
    private int vida=0;
    private int vidaAI=0;
    
    private void setarInimigos(){
        
          
        for (int i=0; i<numZombies; i++){
            Unidade Zombie = new Zombie();
            inimigo.getIntegrantes().add(Zombie);
        }
        zombies.setText(Integer.toString(inimigo.getIntegrantes().size()));
        for (Unidade i : inimigo.getIntegrantes()){
            vidaAI+=i.getPontosDeVida();
        }
         for (Unidade i: inimigo.getIntegrantes()){
            i.setIsNotAI(false);
        }
    }
    
    private void setarAliados(){
        
        for (int i=0; i<Guerreiro.getQtd(); i++){
            Unidade guerreiro = new Guerreiro();
            amigo.getIntegrantes().add(guerreiro);
        }
        for (int i=0; i<Bastiao.getQtd(); i++){
            Unidade bastiao = new Bastiao();
            amigo.getIntegrantes().add(bastiao);
        }
        
        for (int i=0; i<Lanceiro.getQtd(); i++){
            Unidade lanceiro = new Lanceiro();
            amigo.getIntegrantes().add(lanceiro);
        }
        for (int i=0; i<Arqueiro.getQtd(); i++){
            Unidade arqueiro = new Arqueiro();
            amigo.getIntegrantes().add(arqueiro);
        }
        for (Unidade i: amigo.getIntegrantes()){
            i.setIsNotAI(true);
        }
        amigo.atualizarUnidadesCustom();
        vida=amigo.getVidaTotal();
    }
    
    private void setarUI(){
       opacidadeZero();
        
       aliadosGuerreiros.setText(Integer.toString(Guerreiro.getQtd()));
       aliadosLanceiros.setText(Integer.toString(Lanceiro.getQtd()));
       aliadosBastioes.setText(Integer.toString(Bastiao.getQtd()));;
       aliadosArqueiros.setText(Integer.toString(Arqueiro.getQtd()));;
        

      vidaAliadoTotal.setText(Integer.toString(vida));
      vidaAliado.setText(Integer.toString(vida));
      vidaInimigoTotal.setText(Integer.toString(vidaAI));
      vidaInimigo.setText(Integer.toString(vidaAI));
        
    }
    
    private void atualizarUI(){
        int[] tipos=amigo.getTipoIntegrantes();
        int vidaAmigo=0, vidaInimigos=0;
        vidaAmigo=amigo.getVidaTotal();
        aliadosLanceiros.setText(Integer.toString(tipos[2]));
        aliadosBastioes.setText(Integer.toString(tipos[1]));
        aliadosArqueiros.setText(Integer.toString(tipos[3]));
        aliadosGuerreiros.setText(Integer.toString(tipos[0]));
        vidaInimigos=inimigo.getVidaTotal();
        tipos=inimigo.getTipoIntegrantes();
        zombies.setText(Integer.toString(inimigo.getIntegrantes().size()));
        vidaAliado.setText(Integer.toString(vidaAmigo));
        vidaInimigo.setText(Integer.toString(vidaInimigos));
        int vidoca=0;
        for (Unidade i : inimigo.getIntegrantes()){
            if (i instanceof Zombie){
                vidoca+=50;
            }       
        }
        vidaInimigoTotal.setText(Integer.toString(vidoca));
    }
    private void opacidadeZero(){
        bolinha1.setOpacity(0);
        bolinha2.setOpacity(0);
        bolinha3.setOpacity(0);
        bolinha4.setOpacity(0);
        bolinha5.setOpacity(0);
        
        cura=false;
        Guerreiro.setMovimentoEspecial(false);
        Arqueiro.setMovimentoEspecial(false);
        Bastiao.setMovimentoEspecial(false);
        Lanceiro.setMovimentoEspecial(false);
    }
    @FXML
    private void habilidadeGuerreiro(){
        if (Guerreiro.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Guerreiro.setMovimentoEspecial(true);
            bolinha1.setOpacity(1);
        }
    }
    @FXML
    private void habilidadeLanceiro(){
        if (Lanceiro.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Lanceiro.setMovimentoEspecial(true);
            bolinha2.setOpacity(1);
        }
    }
    @FXML
    private void habilidadeBastioes(){
        if (Bastiao.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Bastiao.setMovimentoEspecial(true);
            bolinha3.setOpacity(1);
        }
    }
    @FXML
    private void habilidadeArqueiro(){
        if (Arqueiro.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Arqueiro.setMovimentoEspecial(true);
            bolinha4.setOpacity(1);
        }
    }
    @FXML
    private void hablidadeCura(){
        if (cura)
            opacidadeZero();
        else{
            opacidadeZero();
            cura=true;
            bolinha5.setOpacity(1);
        }
    }
    
    @FXML
    private void proximoTurno(){
       if (Bastiao.isMovimentoEspecial()&&numBastiaoEspecial<10&&contadorBastiao>=100){
           contadorBastiao-=100;
           numBastiaoEspecial++;
           for (Unidade i: amigo.getIntegrantes()){
               if (i instanceof Bastiao){
                   i.setPontosDeVida(i.getPontosDeVida()+3);
                   i.setArmadura(i.getArmadura()-3);
               }
           }
       }
       if (cura&&contadorCura>=50){
           contadorCura-=50;
              if(amigo.getIntegrantes().size()>50){
                Random random = new Random();
                for(int z=0; z<50; z++){
                    amigo.getIntegrantes().get(z).setPontosDeVida(amigo.getIntegrantes().get(z).getPontosDeVida()+1);
                }
               }
              else{
                 for(int z=0; z<amigo.getIntegrantes().size(); z++){
                    amigo.getIntegrantes().get(z).setPontosDeVida(amigo.getIntegrantes().get(z).getPontosDeVida()+1);
                } 
              }
        }
            int v = Integer.parseInt(velocidade.getText());
            if (v<=0){
                v=1;
            }
            for (int i=0; i<v; i++){
                contadorBastiao++;
                contadorCura++;
               if (amigo.getIntegrantes().size()>=1&&inimigo.getIntegrantes().size()>=1){
                    Random random = new Random();
                    int a = random.nextInt(inimigo.getIntegrantes().size());
                    int b = random.nextInt(amigo.getIntegrantes().size());
                    inimigo.receberGolpe(amigo.getIntegrantes().get(b), a);
                    if (amigo.getIntegrantes().size()>=1&&inimigo.getIntegrantes().size()>=1){
                        a = random.nextInt(amigo.getIntegrantes().size());
                        b = random.nextInt(inimigo.getIntegrantes().size());
                        if(amigo.receberGolpe(inimigo.getIntegrantes().get(b), a)){
                            inimigo.getIntegrantes().add(new Zombie());
                            contadorZombies++;
                        }
                        
                    }
               }
               if (inimigo.getIntegrantes().size()<=0){
                   v=0;
                   vitoriaOuDerrota(true);

               }
               if (amigo.getIntegrantes().size()<=0){
                   v=0;
                   vitoriaOuDerrota(false);
               }
             atualizarUI(); 
        }

    }
    private void limpaCoisas(){
        Lanceiro.setQtd(0);
        Arqueiro.setQtd(0);
        Guerreiro.setQtd(0);
        Bastiao.setQtd(0);
        clip.stop();
    }
    private void vitoriaOuDerrota(boolean a){
        if(a)
            TelaVitoriaeDerrotaController.setVitoria(true);
        else
            TelaVitoriaeDerrotaController.setVitoria(false);
        
        int tipos[] = amigo.getTipoIntegrantes();
        int MGA=Guerreiro.getQtd()-tipos[0];
        int MBA=Bastiao.getQtd()-tipos[1];
        int MLA=Lanceiro.getQtd()-tipos[2];
        int MAA=Arqueiro.getQtd()-tipos[3];
        TelaVitoriaeDerrotaController.setTemZombi(true);
        TelaVitoriaeDerrotaController.setInizombie(contadorZombies-inimigo.getIntegrantes().size());
        TelaVitoriaeDerrotaController.setMortosAliados(MGA, MBA, MLA, MAA);
        limpaCoisas();
        TrabalhoEstrategia.trocaTela("TelaVitoriaeDerrota.fxml");
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setarInimigos();
        setarAliados();
        setarUI(); 
        clip= new AudioClip(AUDIO_URL);
        clip.play(0.5);
    }    
    
}
