/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe principal do trabalho - Subclasse de Application -
 * @author Aluno
 */
public class TrabalhoEstrategia extends Application {
    
    private static Stage stage;

    /**
     * Retorna a fase
     * @return
     */
    public static Stage getStage() {
        return stage;
    }
    
    /**
     * Troca de tela
     * @param tela
     */
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(TrabalhoEstrategia.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Starta o menu
     * @param stage
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        
        Scene scene = new Scene(root);
        //observar essa linha = 
        TrabalhoEstrategia.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     *  Inicializa
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
