/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import trabalhoestrategia.model.Lanceiro;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class LanceiroPersonalizarController implements Initializable {

    int numPontos=10;
    
    @FXML
    private Label vida;
    @FXML
    private Label dano;
    @FXML
    private Label pontos;
    @FXML
    private void maisVida(){
        if (numPontos>0){
        Lanceiro.maisVida();
        numPontos--;
        danoEVida();
    }
    }
    private void danoEVida(){
        vida.setText(Integer.toString(6+Lanceiro.getPerVida()));
        dano.setText(Integer.toString(5+Lanceiro.getPerAtaque()));
        pontos.setText(Integer.toString(numPontos));
    }
       
    @FXML
    private void menosVida(){
        if (Lanceiro.getPerVida()>0||Lanceiro.getPerVida()>-5){
            Lanceiro.menosVida();
            numPontos++;
        }
        danoEVida();
    
    }
    @FXML
    private void maisAtaque(){
        if (numPontos>0){
        Lanceiro.maisAtaque();
        numPontos--;
        danoEVida();
    }
    
    }
    @FXML
    private void menosAtaque(){
         if (Lanceiro.getPerAtaque()>0||Lanceiro.getPerAtaque()>-4){
            Lanceiro.menosAtaque();
            numPontos++;
            danoEVida();
        }
    
    }
    @FXML
    private void trocarExercito(){
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vida.setText(Integer.toString(Lanceiro.getPerVida()+Lanceiro.getVidaNormal()));
        dano.setText(Integer.toString(Lanceiro.getPerAtaque()+Lanceiro.getDanoNormal()));
        numPontos = 10-Lanceiro.getPerVida()-Lanceiro.getPerAtaque();
        pontos.setText(Integer.toString(numPontos));
    }    
   
    
}
