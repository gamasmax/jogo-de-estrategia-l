/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

import trabalhoestrategia.model.Unidade;

/**
 * Classe com os atributos e métodos relacionados aos arqueiros
 * - Subclasse de unidade -
 * @author Aluno
 */
public class Arqueiro extends Unidade{

    private static int perVida = 0;
    private static int perAtaque = 0;
    private static int qtd=0;
    private static boolean movimentoEspecial = false;
   
    /**
     *Seta os atributos do arqueiro
     */
    public Arqueiro(){
        this.setPontosDeVida(7);
        this.setArmadura(3);
        this.setPenetracaoDeArmadura(0);
        this.setDanoPorAtaque(8);
        this.setLongRange(true);
    }

    /**
     * Retorna o dano do arqueiro
     * @return 
     */
    public static int getDanoNormal(){
        return 8;
    }

    /**
     * Retorna a vida do arqueiro
     * @return
     */
    public static int getVidaNormal(){
        return 7;
    }

    /**
     * Retorna a quantidade de arqueiros
     * @return
     */
    public static int getQtd() {
        return Arqueiro.qtd;
    }

    /**
     * Seta a quantidade de arqueiros
     * @param qtd
     */
    public static void setQtd(int qtd) {
        Arqueiro.qtd = qtd;
    }
    
    /**
     * Incrementa a vida dos arqueiros
     */
    public static void maisVida(){
        ++perVida;
    }

    /**
     * Diminui a vida dos arqueiros
     */
    public static void menosVida(){
        --perVida;
    }

    /**
     * Incrementa o ataque dos arqueiros
     */
    public static void maisAtaque(){
        ++perAtaque;
    }

    /**
     * Diminui o ataque dos arqueiros
     */
    public static void menosAtaque(){
        --perAtaque;
    }

    /**
     * Retorna a vida adicional dos arqueiros
     * @return
     */
    public static int getPerVida() {
        return perVida;
    }

    /**
     * Seta a vida adicional dos arqueiros
     * @param perVida
     */
    public static void setPerVida(int perVida) {
        Arqueiro.perVida = perVida;
    }

    /**
     * Retorna o ataque adicional dos arqueiros
     * @return
     */
    public static int getPerAtaque() {
        return perAtaque;
    }

    /**
     * Seta o ataque adicional dos arqueiros
     * @param perAtaque
     */
    public static void setPerAtaque(int perAtaque) {
        Arqueiro.perAtaque = perAtaque;
    }

    /**
     * Seta o dano real do arqueiro, com base nas modificações do usuário
     * @return
     */
    public int getDanoPorAtaqueCustom() {
        return danoPorAtaque+perAtaque;
    }

    /**
     * Seta a vida real do arqueiro, com base nas modificações do usuário
     * @return
     */
    public int getPontosDeVidaCustom() {
        return pontosDeVida+perVida;
    }
    
    @Override
    public String toString() {
        return "Atributos: " + "Pontos De Vida: " + pontosDeVida + "; Armadura: " + armadura + "; Penetração De Armadura: " + penetracaoDeArmadura + "; Dano Por Ataque: " + danoPorAtaque + "; Consegue Atacar de Longe: " + longRange + "; Custo Para Recrutar: ";
    }
    
    /**
     * Retorna o estado do movimento especial do bastiao
     * @return
     */
    public static boolean isMovimentoEspecial() {
        return Arqueiro.movimentoEspecial;
    }

    /**
     * Seta o movimento especial do arqueiro
     * @param a
     */
    public static void setMovimentoEspecial(boolean a) {
        Arqueiro.movimentoEspecial = a;
    }
    
    /**
     * Retorna se o arqueiro recebe dano, com base na vida restante
     * @param a 
     * @return 
     */
    @Override
    public boolean receberDano (Unidade a){
       if (pontosDeVida<=0){
            return true;
        }
        else{
        this.pontosDeVida-=calculoDano(a);
        if (this.pontosDeVida<=0)
            return true;
        else
            return false;
        } 
    }
    
    /**
     * Calcula o dano recebido pelo arqueiro, 
     * baseado nos atributos de ataque do inimigo, e os de defesa da unidade
     * @param a
     * @return
     */
    
   
   
}
