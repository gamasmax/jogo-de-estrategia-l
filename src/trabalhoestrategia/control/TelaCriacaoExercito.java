/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import trabalhoestrategia.model.Arqueiro;
import trabalhoestrategia.model.Bastiao;
import trabalhoestrategia.model.Guerreiro;
import trabalhoestrategia.model.Lanceiro;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
/**
 * Controlador da tela de criação do exército
 * @author Aluno
 */
public class TelaCriacaoExercito implements Initializable {
    
    /**
     *
     */
    public static int escolha=0;

    /**
     *
     */
    public static int recursosAtuais=0;

    /**
     *
     */
    public static boolean batalhaEntrada=false;

    /**
     * Retorna o estado da tela batalha
     * @return
     */
    public static boolean isBatalhaEntrada() {
        return batalhaEntrada;
    }

    /**
     * Entra e sai da tela batalha
     * @param batalhaEntrada
     */
    public static void setBatalhaEntrada(boolean batalhaEntrada) {
        TelaCriacaoExercito.batalhaEntrada = batalhaEntrada;
    }
    private int recursos = 0;
    private int valorGuerreiro=2;
    private int valorBastiao=7;
    private int valorArqueiro=4;
    private int valorLanceiro=4;
    @FXML
    private TextField textGuerreiro;
    @FXML
    private TextField textBastiao;
    @FXML
    private TextField textArqueiro;
    @FXML
    private TextField textLanceiro;
    @FXML
    private Label pontos;
    
    @FXML
    private void maisDezGuerreiros(){
        int a = Integer.parseInt(textGuerreiro.getText());
        if (!((recursos-10*valorGuerreiro)<0)){   
        recursos=recursos-10*valorGuerreiro;
        a=a+10;
        textGuerreiro.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void maisUmGuerreiro(){
        int a = Integer.parseInt(textGuerreiro.getText());
        if (!((recursos-1*valorGuerreiro)<0)){   
        recursos=recursos-1*valorGuerreiro;
        a=a+1;
        textGuerreiro.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void menosDezGuerreiros(){
        int a = Integer.parseInt(textGuerreiro.getText());
        if (a-10<0)
            a=a;
        else{
            a=a-10;
            pontos.setText("Recursos Restantes: " + recursos);
            recursos=recursos+10*valorGuerreiro;
        }
        textGuerreiro.setText(Integer.toString(a));
    }
    @FXML
    private void menosUmGuerreiro(){
        int a = Integer.parseInt(textGuerreiro.getText());
        if (a-1<0)
            a=a;
        else{    
            recursos=recursos+1*valorGuerreiro;
             a=a-1; 
              pontos.setText("Recursos Restantes: " + recursos);
        }
        textGuerreiro.setText(Integer.toString(a));
    }
     @FXML
    private void maisDezBastiao(){
        int a = Integer.parseInt(textBastiao.getText());
        if (!((recursos-10*valorBastiao)<0)){   
        recursos=recursos-10*valorBastiao;
        a=a+10;
        textBastiao.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void maisUmBastiao(){
        int a = Integer.parseInt(textBastiao.getText());
        if (!((recursos-1*valorBastiao)<0)){   
        recursos=recursos-1*valorBastiao;
        a=a+1;
        textBastiao.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void menosDezBastiao(){
         int a = Integer.parseInt(textBastiao.getText());
        if (a-10<0)
            a=a;
        else{
           recursos=recursos+10*valorBastiao;   
            a=a-10;
            pontos.setText("Recursos Restantes: " + recursos);
        }
        textBastiao.setText(Integer.toString(a));
    }
    @FXML
    private void menosUmBastiao(){
        int a = Integer.parseInt(textBastiao.getText());
        if (a-1<0)
            a=a;
        else{    
            recursos=recursos+1*valorBastiao;
            a=a-1; 
            pontos.setText("Recursos Restantes: " + recursos);
        }
        textBastiao.setText(Integer.toString(a));
    }
    
    @FXML
    private void maisDezArqueiro(){
        int a = Integer.parseInt(textArqueiro.getText());
        if (!((recursos-10*valorArqueiro)<0)){   
        recursos=recursos-10*valorArqueiro;
        a=a+10;
        textArqueiro.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void maisUmArqueiro(){
        int a = Integer.parseInt(textArqueiro.getText());
        if (!((recursos-1*valorArqueiro)<0)){   
        recursos=recursos-1*valorArqueiro;
        a=a+1;
        textArqueiro.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void menosDezArqueiro(){
        int a = Integer.parseInt(textArqueiro.getText());
        if (a-10<0)
            a=a;
        else{
           recursos=recursos+10*valorArqueiro;   
            a=a-10;
            pontos.setText("Recursos Restantes: " + recursos);
        }
        textArqueiro.setText(Integer.toString(a));
    }
    @FXML
    private void menosUmArqueiro(){
        int a = Integer.parseInt(textArqueiro.getText());
        if (a-1<0)
            a=a;
        else{    
            recursos=recursos+1*valorArqueiro;
            a=a-1; 
            pontos.setText("Recursos Restantes: " + recursos);
        }
        textArqueiro.setText(Integer.toString(a));
    }
    
    
    @FXML
    private void maisDezLanceiro(){
        int a = Integer.parseInt(textLanceiro.getText());
        if (!((recursos-10*valorLanceiro)<0)){   
        recursos=recursos-10*valorLanceiro;
        a=a+10;
        textLanceiro.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void maisUmLanceiro(){
        int a = Integer.parseInt(textLanceiro.getText());
        if (!((recursos-1*valorLanceiro)<0)){   
        recursos=recursos-1*valorLanceiro;
        a=a+1;
        textLanceiro.setText(Integer.toString(a));
        pontos.setText("Recursos Restantes: " + recursos);
        }
    }
    @FXML
    private void menosDezLanceiro(){
        int a = Integer.parseInt(textLanceiro.getText());
        if (a-10<0)
            a=a;
        else{
           recursos=recursos+10*valorLanceiro;   
            a=a-10;
            pontos.setText("Recursos Restantes: " + recursos);
        }
        textLanceiro.setText(Integer.toString(a));
    }
    @FXML
    private void menosUmLanceiro(){
        int a = Integer.parseInt(textLanceiro.getText());
        if (a-1<0)
            a=a;
        else{    
            recursos=recursos+1*valorLanceiro;
            a=a-1; 
            pontos.setText("Recursos Restantes: " + recursos);
        }
        textLanceiro.setText(Integer.toString(a));
    }
    
   
    
    @FXML
    private void trocarTelaBatalha(){
        setarQtd();
        TelaCriacaoExercito.setBatalhaEntrada(false);
        if(escolha!=4){
        TrabalhoEstrategia.trocaTela("Batalha.fxml");
        }
        else{
            TrabalhoEstrategia.trocaTela("BatalhaFinal.fxml");
        }
    }
    private void setarQtd(){
        Lanceiro.setQtd(Integer.parseInt(textLanceiro.getText()));
        Guerreiro.setQtd(Integer.parseInt(textGuerreiro.getText()));
        Arqueiro.setQtd(Integer.parseInt(textArqueiro.getText()));
        Bastiao.setQtd(Integer.parseInt(textBastiao.getText()));
    }
    @FXML
    private void trocarTelaGuerreiro(){
        setarQtd();
        TelaCriacaoExercito.setBatalhaEntrada(false);
        TrabalhoEstrategia.trocaTela("GuerreiroPersonalizar.fxml");
    }
    @FXML
    private void trocarTelaLanceiro(){
        setarQtd();
        TelaCriacaoExercito.setBatalhaEntrada(false);
        TrabalhoEstrategia.trocaTela("LanceiroPersonalizar.fxml");
    }
    @FXML
    private void trocarTelaArqueiro(){
        setarQtd();
        TelaCriacaoExercito.setBatalhaEntrada(false);
        TrabalhoEstrategia.trocaTela("ArqueiroPersonalizar.fxml");
    }
    @FXML
    private void trocarTelaBastiao(){
        setarQtd();
        TelaCriacaoExercito.setBatalhaEntrada(false);
        TrabalhoEstrategia.trocaTela("BastiaoPersonalizar.fxml");
    }

    /**
     * Retorna a batalha escolhida
     * @return
     */
    public static int getEscolha() {
        return escolha;
    }

    /**
     * Seta a batalha escolhida
     * @param escolha
     */
    public static void setEscolha(int escolha) {
        TelaCriacaoExercito.escolha = escolha;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        textGuerreiro.setText(Integer.toString(Guerreiro.getQtd()));
        textArqueiro.setText(Integer.toString(Arqueiro.getQtd()));
        textLanceiro.setText(Integer.toString(Lanceiro.getQtd()));
        textBastiao.setText(Integer.toString(Bastiao.getQtd()));
        if (escolha==0&&batalhaEntrada==true){
            recursosAtuais=400;
            recursos=400;
        }
        else if (escolha==1&&batalhaEntrada==true){
            recursosAtuais=1500;
            recursos=1500;
        }
        else if(escolha==2&&batalhaEntrada==true){
            recursosAtuais=2100;
            recursos=2100;
        }
        else if(escolha==3&&batalhaEntrada==true){
            recursosAtuais=1000;
            recursos=1000;
        }
        else if(escolha==4&&batalhaEntrada==true){
            recursosAtuais=2000;
            recursos=2000;
        }
        
        else{

            recursos=recursosAtuais-((valorGuerreiro*Guerreiro.getQtd())+(valorBastiao*Bastiao.getQtd())+(valorArqueiro*Arqueiro.getQtd())+(valorLanceiro*Lanceiro.getQtd()));
        }
        pontos.setText("Recursos Restantes: " + recursos);
      }   
    
}
