/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

import trabalhoestrategia.model.Unidade;

/**
 * Classe com os atributos e métodos relacionados aos guerreiros
 * - Subclasse de unidade -
 * @author Aluno
 */
public class Guerreiro extends Unidade{

    private static int perVida = 0;
    private static int perAtaque = 0;
    private static int qtd=0;
    private static boolean movimentoEspecial = false;
    
    /**
     * Seta os atributos do guerreiro
     */
    public Guerreiro(){
        this.setPontosDeVida(10);
        this.setArmadura(3);
        this.setPenetracaoDeArmadura(2);
        this.setDanoPorAtaque(3);
        this.setLongRange(false);
    }

    /**
     * Retorna a quantidade de guerreiros
     * @return
     */
    public static int getQtd() {
        return Guerreiro.qtd;
    }

    /**
     * Seta a quantidade de guerreiros
     * @param qtd
     */
    public static void setQtd(int qtd) {
        Guerreiro.qtd = qtd;
    }
    
    /**
     * Incrementa a vida dos guerreiros
     */
    public static void maisVida(){
        ++perVida;
    }

    /**
     * Diminui a vida dos guerreiros
     */
    public static void menosVida(){
        --perVida;
    }

    /**
     * Incrementa o ataque dos guerreiros
     */
    public static void maisAtaque(){
        ++perAtaque;
    }

    /**
     * Diminui o ataque dos guerreiros
     */
    public static void menosAtaque(){
        --perAtaque;
    }

    /**
     * Retorna a vida adicional dos guerreiros
     * @return
     */
    public static int getPerVida() {
        return perVida;
    }

    /**
     * Seta a vida adicional dos guerreiros
     * @param perVida
     */
    public static void setPerVida(int perVida) {
        Guerreiro.perVida = perVida;
    }

    /**
     * Retorna o ataque adicional dos guerreiros
     * @return
     */
    public static int getPerAtaque() {
        return perAtaque;
    }

    /**
     * Seta o ataque adicional dos guerreiros
     * @param perAtaque
     */
    public static void setPerAtaque(int perAtaque) {
        Guerreiro.perAtaque = perAtaque;
    }

    /**
     * Estabelece o ataque real dos guerreiros, pela soma do dano base mais incrementos
     * @return
     */
    public int getDanoPorAtaqueCustom() {
        return danoPorAtaque+perAtaque;
    }

    /**
     * Estabelece a vida real dos guerreiros, pela soma do dano base mais incrementos
     * @return
     */
    public int getPontosDeVidaCustom() {
        return pontosDeVida+perVida;
    }
        @Override
    public String toString() {
        return "Atributos: " + "Pontos De Vida: " + pontosDeVida + "; Armadura: " + armadura + "; Penetração De Armadura: " + penetracaoDeArmadura + "; Dano Por Ataque: " + danoPorAtaque + "; Consegue Atacar de Longe: " + longRange + "; Custo Para Recrutar: ";
    }

    /**
     * Retorna o dano base dos guerreiros
     * @return
     */
    public static int getDanoNormal(){
        return 3;
    }

    /**
     * Retorna a vida base dos guerreiros
     * @return
     */
    public static int getVidaNormal(){
        return 10;
    }

    /**
     * Retorna o status do movimento especial do guerreiro
     * @return
     */
    public static boolean isMovimentoEspecial() {
        return Guerreiro.movimentoEspecial;
    }

    /**
     * Seta o movimento especial dos guerreiros
     * @param a
     */
    public static void setMovimentoEspecial(boolean a) {
        Guerreiro.movimentoEspecial = a;
    }

    /**
     * Verifica se o guerreiro pode receber dano
     * @param a
     * @return
     */
    @Override
    public boolean receberDano (Unidade a){
       if (pontosDeVida<=0){
            return true;
        }
        else{
        if (Guerreiro.isMovimentoEspecial()==true&&isNotAI==true)
            this.pontosDeVida-=calculoDano(a)*2;
        else
            this.pontosDeVida-=calculoDano(a);
        if (this.pontosDeVida<=0)
            return true;
        else
            return false;
        } 
    }

    /**
     * Retorna o dano recebido pelo guerreiro
     * @param a
     * @return
     */
   
}
