# Trabalho de programação -  jogo de estratégias

## Video de apresentação do trabalho:

https://www.youtube.com/watch?v=vzAWRou7twU&feature=youtu.be

## Visão geral do jogo

O jogo é separado por turnos, onde o jogador decide criar unidades, usar habilidades especiais, entre outros.
A cada 3 turnos, os jogadores podem usar uma habilidade especial dos seus guerreiros, podendo assim mudar o rumo da batalha.
O jogador controla as tropas e escolhe o ataque que deseja realizar.
Por meio de batalhas, conquistas e trocas, o jogador ganha recursos para criar novas unidades e manter seu império.

## Funcionamento: 

1. O usuário é apresentado a uma tela com 4 unidades diferentes podendo criar diferentes quantidades dessas unidades;
2. Acima do botão de criar unidades há um botão para personalizar os diferentes tipos de unidade que direcionará o usuário para uma tela alternativa;
3. Na tela de personalização, o usuário poderá dividir 10 pontos de habilidade entre a vida e o dano base da unidade selecionada;
4. Com as unidades criadas, o usuário poderá entrar na tela de batalha, através da tela inicial, onde ele será redirecionado para outra tela;
5. Na tela de batalha, serão apresentados os valores de batalha criados pelo usuário, como vida e dano de cada unidade, assim como do exército.

## Classes:

- **Arqueiro** - Combatente de longo alcance, capaz de causar muito dano se bem protegido 
por unidades aliadas.
> "De que vale uma espada se você não pode usá-la?"
![](https://3.bp.blogspot.com/-pmbT3x1X7Sg/V5QKGTNMebI/AAAAAAAAAEI/UO7uCsj5HeobWd0FyIZKy_Bgkc9ICFy2ACLcB/s400/0347ef_9c882345b2d9407cab6f9e12aaa65fbe.jpeg)
- **Bastião** - Experiente unidade capaz de causar grandes quantidades de dano, além de possuir
uma forte armadura resistente ao dano das linhas de frente inimigas.
> "A espada pressente a guerra!
![](https://i.pinimg.com/474x/c6/2e/57/c62e57dec708d22a75292a672d8e45bb.jpg?b=t)
- **Guerreiro** - Unidade de linha de frente capaz de resistir a pequenas quantias de dano
e retribuir o dano recebido com sua espada de ferro.
> "Pela guerra!"
![](https://pm1.narvii.com/6749/f5a65645a61ce0bd3ec4056056bb78b6521cd760v2_hq.jpg)
- **Lanceiro** - Unidade com força suficiente para transformar armaduras em pó, e causar grandes danos
em seus adversários
> "Lanceiros, avante!"
![](https://i.pinimg.com/originals/21/a5/ab/21a5ab659b09da7237ddf17d227778b4.jpg)

## Stats:

|                      | Arqueiro | Bastião  | Guerreiro | Lanceiro |Morto-vivo|
|----------------------|----------|----------|-----------|----------|----------|
|         Vida         |     7    |    15    |    10     |    6     |     ?    |
|       Armadura       |     3    |    6     |    3      |    2     |     ?    |
|Penetração de armadura|     0    |    3     |    2      |    5     |     ?    |
|    Dano por ataque   |     8    |    6     |    3      |    5     |     ?    |
|       Alcance        |   Longo  |   Curto  |   Curto   |   Curto  |     ?    |
|    Golpe especial    |  Alvejar |Redemoinho|   Fúria   |  Furtivo |     ?    |
|Custo de recrutamento |     4    |    7     |    2      |    4     |     ?    |

## Golpes especiais:

- **Headshot** - O arqueiro acerta a cabeça de seu adversario causando mais dano
- **Retirar armadura** - O bastião perde 3 de armadura, e ganha 3 de vida em troca
- **Enfurecer** - O guerreiro se enche de ira e tem seu dano e dano recebido dobrado pelo turno
- **Cortar** - O lanceiro se esgueira e aumenta sua penetração de armadura

## Progresso:

- [x] Desenvolver as classes
- [x] Criar interface gráfica
- [x] Implementar a criação de unidades
- [x] Implementar os golpes especiais
- [x] Funcionamento geral do programa
- [x] Readme
- [x] Revisão e equilíbrio das classes

## Dificuldades encontradas no decorrer da aula:

- Criação do arquivo readme, visto que fora o primeiro contato do grupo com o formato;
> O professor nos auxiliou instruindo como proceder na criação do arquivo através da plataforma do NetBeans
- Desenvolvimento das classes ao redor do tema;
> O professor auxiliou com opiniões a respeito das unidades e do jogo em geral
- Visualização geral da lógica necessária para a criação do jogo;
> O professor auxiliou com comentários gerais acerca da criação das funcões do programa