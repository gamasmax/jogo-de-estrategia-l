/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import java.util.Random;
/**
 * FXML Controller class
 *
 * @author user
 */
public class MenuController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML 
    private void trocarEscolhaBatalha(){
        TrabalhoEstrategia.trocaTela("TelaEscolhaBatalha.fxml");
    }
    @FXML
    private void sair(){
        java.lang.System.exit(0);
    }
    @FXML
    private void sobre(){
        TrabalhoEstrategia.trocaTela("TelaSobre.fxml");
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    
}
