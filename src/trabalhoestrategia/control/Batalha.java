/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import java.awt.Rectangle;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;

import trabalhoestrategia.model.Exercito;
import trabalhoestrategia.model.Unidade;
import trabalhoestrategia.model.Bastiao;
import trabalhoestrategia.model.Guerreiro;
import trabalhoestrategia.model.Lanceiro;
import trabalhoestrategia.model.Arqueiro;
/**
 * FXML Controller class
 *
 * @author Raynan
 */
public class Batalha implements Initializable {

    /**
     * Initializes the controller class.
     */    
    
    @FXML
    private Label vidaAliado;
    @FXML
    private Label vidaInimigo;
    @FXML
    private Label vidaAliadoTotal;
    @FXML
    private Label vidaInimigoTotal;
   
    @FXML
    private Circle bolinha1;
    @FXML
    private Circle bolinha2;
    @FXML
    private Circle bolinha3;
    @FXML
    private Circle bolinha4;
    
    @FXML
    private TextField velocidade; 
    
    @FXML
    private Label aliadosGuerreiros;
    @FXML
    private Label aliadosLanceiros;
    @FXML
    private Label aliadosBastioes;
    @FXML
    private Label aliadosArqueiros;
            
    @FXML
    private Label inimigosGuerreiros;
    @FXML
    private Label inimigosLanceiros;
    @FXML
    private Label inimigosBastioes;
    @FXML
    private Label inimigosArqueiros;
    
    int MGA=0, MAA=0, MBA=0, MLA=0, MGI=0, MAI=0, MBI=0, MLI=0;
    
    private int contadorBastiao=101;
    private int numG=0;
    private int numB=0;
    private int numL=0;
    private int numA=0;
    private int numBastiaoEspecial=0;
    private Exercito amigo = new Exercito();
    private Exercito inimigo = new Exercito();
 
    private int vida=0;
    private int vidaAI=0;
    
    private void setarInimigos(){
        
          
        for (int i=0; i<numG; i++){
            Unidade guerreiro = new Guerreiro();
            inimigo.getIntegrantes().add(guerreiro);
        }
        for (int i=0; i<numB; i++){
            Unidade bastiao = new Bastiao();
            inimigo.getIntegrantes().add(bastiao);
        }
        for (int i=0; i<numL; i++){
            Unidade lanceiro = new Lanceiro();
            inimigo.getIntegrantes().add(lanceiro);
        }
        for (int i=0; i<numA; i++){
            Unidade arqueiro = new Arqueiro();
            inimigo.getIntegrantes().add(arqueiro);
        }
        for (Unidade i : inimigo.getIntegrantes()){
            vidaAI+=i.getPontosDeVida();
        }
        for (Unidade i: inimigo.getIntegrantes()){
            i.setIsNotAI(false);
        }
    }
    
    private void setarAliados(){
        
        for (int i=0; i<Guerreiro.getQtd(); i++){
            Unidade guerreiro = new Guerreiro();
            amigo.getIntegrantes().add(guerreiro);
        }
        for (int i=0; i<Bastiao.getQtd(); i++){
            Unidade bastiao = new Bastiao();
            amigo.getIntegrantes().add(bastiao);
        }
        
        for (int i=0; i<Lanceiro.getQtd(); i++){
            Unidade lanceiro = new Lanceiro();
            amigo.getIntegrantes().add(lanceiro);
        }
        for (int i=0; i<Arqueiro.getQtd(); i++){
            Unidade arqueiro = new Arqueiro();
            amigo.getIntegrantes().add(arqueiro);
        }
        for (Unidade i: amigo.getIntegrantes()){
            i.setIsNotAI(true);
        }
        amigo.atualizarUnidadesCustom();
        vida=amigo.getVidaTotal();
    }
    
    private void setarUI(){
       opacidadeZero();
        
       aliadosGuerreiros.setText(Integer.toString(Guerreiro.getQtd()));
       aliadosLanceiros.setText(Integer.toString(Lanceiro.getQtd()));
       aliadosBastioes.setText(Integer.toString(Bastiao.getQtd()));;
       aliadosArqueiros.setText(Integer.toString(Arqueiro.getQtd()));;
        
        int[] tipos=inimigo.getTipoIntegrantes();
        inimigosGuerreiros.setText(Integer.toString(tipos[0]));
        inimigosLanceiros.setText(Integer.toString(tipos[2]));
        inimigosBastioes.setText(Integer.toString(tipos[1]));
        inimigosArqueiros.setText(Integer.toString(tipos[3]));    
      
      vidaAliadoTotal.setText(Integer.toString(vida));
      vidaAliado.setText(Integer.toString(vida));
      vidaInimigoTotal.setText(Integer.toString(vidaAI));
      vidaInimigo.setText(Integer.toString(vidaAI));
        
    }
    
    private void atualizarUI(){
        int[] tipos=amigo.getTipoIntegrantes();
        int vidaAmigo=0, vidaInimigos=0;
        vidaAmigo=amigo.getVidaTotal();
        aliadosLanceiros.setText(Integer.toString(tipos[2]));
        aliadosBastioes.setText(Integer.toString(tipos[1]));
        aliadosArqueiros.setText(Integer.toString(tipos[3]));
        aliadosGuerreiros.setText(Integer.toString(tipos[0]));
        vidaInimigos=inimigo.getVidaTotal();
        tipos=inimigo.getTipoIntegrantes();
        inimigosGuerreiros.setText(Integer.toString(tipos[0]));
        inimigosLanceiros.setText(Integer.toString(tipos[2]));
        inimigosBastioes.setText(Integer.toString(tipos[1]));
        inimigosArqueiros.setText(Integer.toString(tipos[3]));    
        vidaAliado.setText(Integer.toString(vidaAmigo));
        vidaInimigo.setText(Integer.toString(vidaInimigos));
    }
    private void opacidadeZero(){
        bolinha1.setOpacity(0);
        bolinha2.setOpacity(0);
        bolinha3.setOpacity(0);
        bolinha4.setOpacity(0);
        Guerreiro.setMovimentoEspecial(false);
        Arqueiro.setMovimentoEspecial(false);
        Bastiao.setMovimentoEspecial(false);
        Lanceiro.setMovimentoEspecial(false);
    }
    @FXML
    private void habilidadeGuerreiro(){
        if (Guerreiro.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Guerreiro.setMovimentoEspecial(true);
            bolinha1.setOpacity(1);
        }
    }
    @FXML
    private void habilidadeLanceiro(){
        if (Lanceiro.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Lanceiro.setMovimentoEspecial(true);
            bolinha2.setOpacity(1);
        }
    }
    @FXML
    private void habilidadeBastioes(){
        if (Bastiao.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Bastiao.setMovimentoEspecial(true);
            bolinha3.setOpacity(1);
        }
    }
    @FXML
    private void habilidadeArqueiro(){
        if (Arqueiro.isMovimentoEspecial())
            opacidadeZero();
        else{
            opacidadeZero();
            Arqueiro.setMovimentoEspecial(true);
            bolinha4.setOpacity(1);
        }
    }
    
    
    @FXML
    private void proximoTurno(){
       if (Bastiao.isMovimentoEspecial()&&numBastiaoEspecial<10&&contadorBastiao>=100){
           contadorBastiao-=100;
           numBastiaoEspecial++;
           for (Unidade i: amigo.getIntegrantes()){
               if (i instanceof Bastiao){
                   i.setPontosDeVida(i.getPontosDeVida()+3);
                   i.setArmadura(i.getArmadura()-3);
               }
           }
       }
            int v = Integer.parseInt(velocidade.getText());
            if (v<=0){
                v=1;
            }
            for (int i=0; i<v; i++){ 
               contadorBastiao++;
               if (amigo.getIntegrantes().size()>=1&&inimigo.getIntegrantes().size()>=1){
                    Random random = new Random();
                    int a = random.nextInt(inimigo.getIntegrantes().size());
                    int b = random.nextInt(amigo.getIntegrantes().size());
                    inimigo.receberGolpe(amigo.getIntegrantes().get(b), a);
                    if (amigo.getIntegrantes().size()>=1&&inimigo.getIntegrantes().size()>=1){
                        a = random.nextInt(amigo.getIntegrantes().size());
                        b = random.nextInt(inimigo.getIntegrantes().size());
                        amigo.receberGolpe(inimigo.getIntegrantes().get(b), a);
                    }
               }
               if (inimigo.getIntegrantes().size()<=0){
                   v=0;
                   vitoriaOuDerrota(true);

               }
               if (amigo.getIntegrantes().size()<=0){
                   System.out.println("Voce perdeu");
                   v=0;
                   vitoriaOuDerrota(false);
               }
             atualizarUI(); 
        }

    }
    private void vitoriaOuDerrota(boolean a){
        if(a)
            TelaVitoriaeDerrotaController.setVitoria(true);
        else
            TelaVitoriaeDerrotaController.setVitoria(false);
        int tipos[] = amigo.getTipoIntegrantes();
        MGA=Guerreiro.getQtd()-tipos[0];
        MBA=Bastiao.getQtd()-tipos[1];
        MLA=Lanceiro.getQtd()-tipos[2];
        MAA=Arqueiro.getQtd()-tipos[3];
        tipos = inimigo.getTipoIntegrantes();
        MGI=numG-tipos[0];
        MBI=numB-tipos[1];
        
        MLI=numL-tipos[2];
        MAI=numA-tipos[3];
        TelaVitoriaeDerrotaController.setMortos(MGI, MAI, MLI, MBI, MGA, MAA, MLA, MBA);
        limpaCoisas();
        TrabalhoEstrategia.trocaTela("TelaVitoriaeDerrota.fxml");
        
    }
    private void limpaCoisas(){
        Lanceiro.setQtd(0);
        Arqueiro.setQtd(0);
        Guerreiro.setQtd(0);
        Bastiao.setQtd(0);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (TelaCriacaoExercito.getEscolha()==0){
            numG=30;
            numB=10;
            numL=30;
            numA=30;
        }
        else if (TelaCriacaoExercito.getEscolha()==1){
            numG=200;
            numB=0;
            numL=0;
            numA=100;
        }
        else if (TelaCriacaoExercito.getEscolha()==2){
            numG=30;
            numB=150;
            numL=75;
            numA=150;
        }
        else if (TelaCriacaoExercito.getEscolha()==3){
            numG=600;
            numB=0;
            numL=0;
            numA=0;
        }
        setarInimigos();
        setarAliados();
        setarUI();
        
    }    
    
}
