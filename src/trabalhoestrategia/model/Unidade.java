/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

/**
 * Classe que controla a unidade, que herda as outras subclasses
 * @author Aluno
 */
abstract public class Unidade {

    /**
     *
     */
    protected int pontosDeVida = 0;

    /**
     *
     */
    protected int armadura = 0;

    /**
     *
     */
    protected int penetracaoDeArmadura = 0;

    /**
     *
     */
    protected int danoPorAtaque = 0;

    /**
     *
     */
    protected boolean longRange = false;

    /**
     *
     */
    protected  boolean isNotAI = false;
    /**
     *
     */

    /**
     * Vê se é possível receber dano
     * @param a
     * @return
     */
    abstract public boolean receberDano (Unidade a);
    
    /**
     * Calcula o dano recebido
     * @param a
     * @return
     */

    /**
     * Construtor da classe unidade
     */
    public Unidade() {
    }

    /**
     * Retorna os pontos de vida
     * @return
     */
    public int getPontosDeVida() {
        return pontosDeVida;
    }

    /**
     * Seta os pontos de vida
     * @param pontosDeVida
     */
    public void setPontosDeVida(int pontosDeVida) {
        this.pontosDeVida = pontosDeVida;
    }

    /**
     * Retorna a armadura da unidade
     * @return
     */
    public int getArmadura() {
        return armadura;
    }

    /**
     * Seta a armadura da unidade
     * @param armadura
     */
    public void setArmadura(int armadura) {
        this.armadura = armadura;
    }

    /**
     * Retorna a penetração de armadura da unidade
     * @return
     */
    public int getPenetracaoDeArmadura() {
        return penetracaoDeArmadura;
    }

    /**
     * Seta a penetração de armadura da unidade
     * @param penetracaoDeArmadura
     */
    public void setPenetracaoDeArmadura(int penetracaoDeArmadura) {
        this.penetracaoDeArmadura = penetracaoDeArmadura;
    }

    /**
     * Retorna o dano por ataque da unidade
     * @return
     */
    public int getDanoPorAtaque() {
        return danoPorAtaque;
    }

    /**
     * Seta o dano por ataque da unidade
     * @param danoPorAtaque
     */
    public void setDanoPorAtaque(int danoPorAtaque) {
        this.danoPorAtaque = danoPorAtaque;
    }

    /**
     * Verifica se a unidade é long range
     * @return
     */
    public boolean isLongRange() {
        return longRange;
    }

    /**
     * Seta o range da unidade
     * @param longRange
     */
    public void setLongRange(boolean longRange) {
        this.longRange = longRange;
    }

   
    /**
     * Verifica se a unidade é AI
     * @param isNotAI
     */
    public  boolean isIsNotAI() {
        return isNotAI;
    }
    /**
     * Seta se a unidade é ou nao AI
     * @param isNotAI
     */
    public void setIsNotAI(boolean isNotAI) {
        this.isNotAI = isNotAI;
    }
    /**
     * Calcula o dano dado pelas tropas
     * @param calculoDano
     * 
     */
    public int calculoDano(Unidade a){
       
        int armadurita = this.armadura;
         if (a instanceof Arqueiro){
            if(a.isIsNotAI()==true&&Arqueiro.isMovimentoEspecial())
            {
                armadurita=armadurita/5;
            }
        }
         
        int penet = a.getPenetracaoDeArmadura();
        if (a instanceof Lanceiro){
            if(a.isIsNotAI()==true&&Lanceiro.isMovimentoEspecial())
            {
                penet=penet*3;
            }
        }
        int armaduraEfetiva=armadurita-a.getPenetracaoDeArmadura();
        
        if (armaduraEfetiva<=0)
            armaduraEfetiva=0;
        int danoResultante=a.getDanoPorAtaque()-armaduraEfetiva;
        if (danoResultante<=0)
            danoResultante=1;
        if (a.isLongRange())
            danoResultante+=1;
        if (a instanceof Guerreiro){
           if(a.isIsNotAI()==true&&Guerreiro.isMovimentoEspecial())
               danoResultante= danoResultante*2;
        }
        
        return danoResultante;
    }
   
}
