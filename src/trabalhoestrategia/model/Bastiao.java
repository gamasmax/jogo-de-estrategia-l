/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

import trabalhoestrategia.model.Unidade;

/**
 * Classe com os atributos e métodos relacionados aos bastiões
 * - Subclasse de unidade -
 * @author Aluno
 */
public class Bastiao extends Unidade{
    
  
    private static int qtd=0;
    private static int perVida = 0;
    private static int perAtaque = 0;
    private static boolean movimentoEspecial = false;
    
    /**
     * Seta os atributos do bastião
     */
    public Bastiao(){
        this.setPontosDeVida(15);
        this.setArmadura(6);
        this.setPenetracaoDeArmadura(3);
        this.setDanoPorAtaque(6);
        this.setLongRange(false);
    }
  
    /**
     * Retorna a quantidade de bastioes
     * @return
     */
    public static int getQtd() {
        return Bastiao.qtd;
    }

    /**
     * Seta a quantidade de bastioes
     * @param qtd
     */
    public static void setQtd(int qtd) {
        Bastiao.qtd = qtd;
    }
    
    /**
     * Incrementa a vida do bastiao
     */
    public static void maisVida(){
        ++perVida;
    }

    /**
     * Diminui a vida do bastiao
     */
    public static void menosVida(){
        --perVida;
    }

    /**
     * Incrementa o ataque do bastiao
     */
    public static void maisAtaque(){
        ++perAtaque;
    }

    /**
     * Diminui o ataque do bastiao
     */
    public static void menosAtaque(){
        --perAtaque;
    }

    /**
     * Retorna a vida adicional dos bastioes
     * @return
     */
    public static int getPerVida() {
        return perVida;
    }

    /**
     * Seta a vida adicional dos bastioes
     * @param perVida
     */
    public static void setPerVida(int perVida) {
        Bastiao.perVida = perVida;
    }

    /**
     * Retorna o ataque adicional dos bastioes
     * @return
     */
    public static int getPerAtaque() {
        return perAtaque;
    }

    /**
     * Seta o ataque adicional dos bastioes
     * @param perAtaque
     */
    public static void setPerAtaque(int perAtaque) {
        Bastiao.perAtaque = perAtaque;
    }

    /**
     * Estabelece o ataque real dos bastioes, pela soma do dano base mais incrementos
     * @return
     */
    public int getDanoPorAtaqueCustom() {
        return danoPorAtaque+perAtaque;
    }

    /**
     * Estabelece a vida real dos bastioes, pela soma do dano base mais incrementos
     * @return
     */
    public int getPontosDeVidaCustom() {
        return pontosDeVida+perVida;
    }

    /**
     * Seta o dano base dos bastiões
     * @return
     */
    public static int getDanoNormal(){
        return 6;
    }

    /**
     * Retorna a vida base dos bastiões 
     * @return
     */
    public static int getVidaNormal(){
        return 15;
    }
    @Override
    public String toString() {
        return "Atributos: " + "Pontos De Vida: " + pontosDeVida + "; Armadura: " + armadura + "; Penetração De Armadura: " + penetracaoDeArmadura + "; Dano Por Ataque: " + danoPorAtaque + "; Consegue Atacar de Longe: " + longRange + "; Custo Para Recrutar: ";
    }

    /**
     * Retorna o estado do movimento especial do bastiao
     * @return
     */
    public static boolean isMovimentoEspecial() {
        return Bastiao.movimentoEspecial;
    }

    /**
     * Seta o movimento especial do bastiao
     * @param a
     */
    public static void setMovimentoEspecial(boolean a) {
        Bastiao.movimentoEspecial = a;
    }
    
    /**
     * Verifica se o bastiao pode receber dano
     * @param a
     * @return
     */
    @Override
    public boolean receberDano (Unidade a){
       if (pontosDeVida<=0){
            return true;
        }
        else{
        this.pontosDeVida-=calculoDano(a);
        if (this.pontosDeVida<=0)
            return true;
        else
            return false;
        } 
    }
    
    /**
     * Calcula o dano recebido pelo bastião, 
     * baseado nos atributos de ataque do inimigo, e os de defesa da unidade
     * @param a
     * @return
     */
    
}
